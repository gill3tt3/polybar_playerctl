# polybar\_playerctl

![](1.png)
![](2.png)

This is a Bash script that can be called by Polybar to show the currently playing song and artist, as well as giving the user play/pause/next/previous controls.

## Dependancies

* polybar
* playerctl
* Font Awesome (`pacman -S ttf-font-awesome` // `dnf install fontawesome-fonts fontawesome5-fonts`)

## Installation

1. Verify that all of the dependancies are installed
2. Copy `polybar_playerctl.sh` to a folder on your computer
3. Edit `polybar_playerctl.sh` and change the variables under "User Configuration" to your desired values
4. Edit `~/.config/polybar/config.ini`
5. Add the following lines:
    ```
    [module/playerctl]
    type = custom/script
    interval = 0.2
    exec = '/path/to/polybar_playerctl.sh' 2> /dev/null
    ```
6. Add the `playerctl` module to your bar(s)
7. Restart polybar
